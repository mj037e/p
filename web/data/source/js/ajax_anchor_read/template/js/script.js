document.addEventListener('DOMContentLoaded',function(){

    (function(){
        var gnb_list = ['page1','page2'];

        var i = 0;
        var tags = [];
        var li = '';
        while(i<gnb_list.length){
            li = document.createElement('li');
            li.innerHTML = '<a href="#!'+gnb_list[i]+'" onclick="makeRequest(\'./view/'+gnb_list[i]+'.html\')">'+gnb_list[i]+'</a>';
            tags[i] = li;
            document.querySelector('.gnb ul').appendChild(tags[i]);
            i = i + 1;
        };
    })();



});

window.addEventListener("hashchange",function(){
    location_hash();
});
var httpRequest;
function makeRequest(url) {
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) { // IE
        try {
            httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {}
        }
    }

    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHTTP instance');
        return false;
    }
    httpRequest.onreadystatechange = function(){
        if (httpRequest.readyState === 4 ) {
            if(httpRequest.status === 200){
                document.querySelector('.container .section').innerHTML = this.responseText;
            } else {
                alert('There was a problem with the request.');
            }
        }
    };
    httpRequest.open('GET', url);
    httpRequest.send();
}


function location_hash(){
    // console.log(location.hash);
    if(location.hash){
        makeRequest('./view/'+location.hash.substr(2)+'.html');
    }else{
        makeRequest('./view/main.html');
    }

}