$(document).ready(function() {

	$('h1').animate({'top':0,'opacity':1}, 1000, 'easeOutBounce')

	$('.gnb li').each(function(index){

		$(this).delay(index * 100).fadeIn();
	});
	
	$('.gnb li').click(function(){

		var index = $(this).index();
		var posTitle = -( 80 * index );
		var posArrow = 200 * index;

		$('.list').animate({'top': posTitle}, 300, 'easeInOutQuart');
		$('.arrow').animate({'left': posArrow });

		$('.desc li').eq(index).fadeIn();
		$('.desc li').eq(index).siblings().hide();

		$(this).children('img').addClass('hover', 400, 'easeOutElastic');
		$(this).siblings().children('img').removeClass('hover', 400, 'easeOutElastic');
	});

	// $('.gnb li').eq(0).trigger('click');

});