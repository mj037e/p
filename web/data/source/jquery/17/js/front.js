$(document).ready(function() {

	var slider = $('.bxslider').bxSlider({

		pager : false,
		controls : false,

		onSliderLoad : function(currentIndex) {

			$('#first').find('h2').animate({top:0,opacity:1},1000);
			$('#first').find('p').delay(500).animate({left:0,opacity:1},1000);

			$('#first').find('div.list img').each(function(index){

				$(this).delay(index * 500).fadeIn();
			});
		},

		onSlideBefore : function($slideElement, oldIndex, newIndex){ 

			$('#sbenu').find('h2').css({opacity:0,top:-30});
			$('#sbenu').find('p').css({opacity:0,left:-50});
			$('div.list img').hide();
		},

		onSlideAfter : function($slideElement, oldIndex, newIndex){ 

			$slideElement.find('h2').animate({top:0,opacity:1},1000);
			$slideElement.find('p').delay(500).animate({left:0,opacity:1},1000);
		
			$slideElement.find('div.list img').each(function(index){

				$(this).delay(index * 500).fadeIn();
			});
		}

	});

	$('ul.tabs li').click(function(){

		var index = $(this).index();

		slider.goToSlide(index);

	});
	

});