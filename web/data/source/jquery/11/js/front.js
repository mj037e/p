$( document ).ready( function() {

	$('ul.icon li').hover(function(){
		$( this ).stop().animate( { 'border-width': 3 }, 100 );
	},function(){
		$( this ).stop().animate( { 'border-width': 5 }, 100 );
	});

	$('ul.enemy li').hover(function(){
		$( this ).css('opacity',1);
    $( this ).find( 'img' ).stop().animate({ 'top': -82 }, 300);
	},function(){
		$( this ).css('opacity',0.7);
    $( this ).find( 'img' ).stop().animate({ 'top': 110 }, 300);
	}); 	

  $('button').click(function(){

  	var div = $('div.container');
  	var bg = div.hasClass('bg2');

  	if ( !bg ) {
  		div.slideUp(function(){
  			div.addClass('bg2').css('background-image','url(images/bg02.jpg)');
  		});
  		div.slideDown();
  	} else {
  		div.slideUp(function(){
  			div.removeClass('bg2').css('background-image','url(images/bg01.jpg)');
  		});
  		div.slideDown();
  	}

  });

});