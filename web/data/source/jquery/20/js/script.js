$(document).ready(function() {

	// 옵션 설정부분
	var count_option = 3; // 처음보이는 제품의 개수를 정할수 있다.
	var speed_option = 200; // 제품이 슬라이드 되는 속도를 정할수 있다.
	var width_option = 600; // 제품슬라이드가 들어갈 공간의 넓이를 설정할수 있다.
	var product_option = $('ul.watch li').length; // 슬라이드 되는 전체 제품의 개수를 파악한다.

	slide({ slide : width_option , count : count_option , product : product_option });

	$('div.button > div').click(function(){

		var check = $(this).attr('class');
		var move = $('ul.watch li').width();

		if ( check == "prev" ) { 
			prevSlide({ speed : speed_option , ani : move });
		} else {
			nextSlide({ speed : speed_option , ani : move });
		}
	});

	function slide(option){

		$('div.container').css('width',option.slide);

		var product_width = Math.ceil( option.slide / option.count);
		var ul_width = product_width * product_option;

		$('ul.watch li').css('width',product_width);
		$('ul.watch').css('width',ul_width);
	}

	function prevSlide(option){
		
		$('ul.watch').stop().animate({ 'left': '+=' + option.ani }, option.speed , end);    
	}

	function nextSlide(option){
		
		$('ul.watch').stop().animate({ 'left': '-=' + option.ani }, option.speed , end);
	}

	function end(){

    var product = $( 'div.slide' ).width();
    var rolling = $( 'ul.watch' ).width();
    var aimPosition = -( rolling - product );
    var sushiPosition = $( 'ul.watch' ).position().left;

    if( sushiPosition > 0 ) {
        $( 'ul.watch' ).animate({ 'left': 0 },200); 
    } else if( sushiPosition < aimPosition ) {
        $( 'ul.watch' ).animate({ 'left': aimPosition },200); 
    }
	}

});
