$(document).ready(function() {

	var currentHeight = 185;
	var checkPoint = 160;

	$( document ).ready( function() {

		updateHeight();
		
		$( '#arr-top' ).on( 'click', function() {
			
			currentHeight = currentHeight + 1;

			updateHeight();	
		});	

		$( '#arr-bottom' ).on( 'click', function() {
			
			currentHeight = currentHeight - 1;

			updateHeight();	
		});	
	});

	function updateHeight() {
		$( '#height' ).text( currentHeight );

		$( '#leg' ).css( 'height', currentHeight );

		if( checkPoint > currentHeight )
		{
			$( '#face' ).css( 'background-position', 'right' );	

		} else {

			$( '#face' ).css( 'background-position', 'left' );	
		}
	}

});