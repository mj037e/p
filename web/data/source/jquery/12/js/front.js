$(document).ready(function() {

	$('div.bar a').click(function(){
		
		if ( $(this).hasClass('menu01') ){
			$('ul.fashion').hide();
			$('ul.doll').show();
		}

		if ( $(this).hasClass('menu02') ){
			$('ul.doll').hide();
			$('ul.fashion').show();
		}

	});

	$('ul li').click(function(){

		var url = $(this).find('img').attr('src').replace('.jpg','_zoom.jpg');
		var zoom = $('<img>').attr('src',url);
			
		$('div.zoom').empty().append(zoom);
	});

});