$(document).ready(function() {

	$('#fullpage').fullpage({
		
		anchors: ['Page01', 'Page02', 'Page03'],
		
		navigation: true,
		navigationTooltips: ['Page01', 'Page02', 'Page03'],
		
		afterLoad: function(anchorLink, index){

			if (index == 3) {


				$('#section03').find('li').each(function(index){

					$(this).delay(index*100).animate({top:0,opacity:1},500);
				});

			} else {

				$('#section03').find('li').css({opacity:0,top:50});
			}

		}

	});

	$('#section01 img').animate({top:0,opacity:1},2000);

});