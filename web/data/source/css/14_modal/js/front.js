$(document).ready(function() {

	$('button.button').click(function(){
		$('div.blind').fadeIn();
		$('div.box').addClass('in');
	});

	$('button.close_btn, button.close_box').click(function(){
		$('div.box').removeClass('in');
		$('div.blind').fadeOut();
	});

	$('div.blind').click(function(){
		$('div.box').removeClass('in');
		$(this).fadeOut();
	});

	$('div.box').click(function(event){
		event.stopPropagation();
	});

});