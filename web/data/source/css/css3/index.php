<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="generator" content="제작툴"/>
        <meta name="author" content="mari" />
        <meta name="description" content="description" />
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <title>css source</title>
        <link rel="stylesheet" href="css/style.css" />
    </head>
    <body>
	
	<div class="group">
	<h2>border transition</h2>
	<ul>
		<li><a href="data/border_transition/border_transitions.html">ALL</a></li>
		<li><a href="data/border_transition/bar.html">bar</a></li>
		<li><a href="data/border_transition/center.html">center</a></li>
		<li><a href="data/border_transition/curmudgeon.html">curmudgeon</a></li>
		<li><a href="data/border_transition/draw.html">draw</a></li>
		<li><a href="data/border_transition/foo.html">foo</a></li>
		<li><a href="data/border_transition/meet.html">meet</a></li>
		<li><a href="data/border_transition/spin.html">spin</a></li>
		<li><a href="data/border_transition/spinCircle.html">spinCircle</a></li>
		<li><a href="data/border_transition/spinThick.html">spinThick</a></li>
	</ul>
	</div>

	<div class="group">
	<h2>font</h2>
	<ul>
		<li><a href="data/font/3d.html">3D</a></li>
		<li><a href="data/font/text_shadow01.html">text-shadow를 이용한 다른 형태의 그림자</a></li>


	</ul>
	</div>

	<div class="group">
	<h2>font transition</h2>
	<ul>
		<li><a href="data/font/font_effect01.html">텍스트 링크위에 마우스를 올리면 물결 이미지가 부드럽게 올라오는 효과</a></li>
		<li><a href="data/font/font_effect02.html">텍스트 호버 이펙트</a></li>
		<li><a href="data/gradient_text/gradient.html">gradient text</a></li>
		<li><a href="data/gradient_text/gradient1.html">gradient text 1</a></li>
		<li><a href="data/gradient_text/gradient2.html">gradient text 2</a></li>
	</ul>
	</div>


	<div class="group">
	<h2>gnb hover효과</h2>
	<ul>
		<li><a href="data/hover_btn/hover.html">css3 다양한 링크 hover 효과</a></li>
	</ul>
	</div>

	<div class="group">
	<h2>gallery</h2>
	<ul>
		<li><a href="data/gallery/gallery01.html">썸네일 갤러리</a></li>
		<li><a href="data/gallery/gallery02.html">스타일로 구성된 심플한 갤러리 </a></li>
		<li><a href="data/gallery/gallery01.html"></a></li>
	</ul>
	</div>


    </body>
</html>















