var func = (function($){
    
    

    var focusOn= function(){
        $(this).siblings().addClass('active');
        $(this).attr('placeholder','');
    }
    
    var focusOut= function(){
        var  p = ['user id','user password','user password confirm', 'user email','user phone number']
        var index = $('input').index(this);
        $(this).siblings().removeClass('active');
        $(this).attr('placeholder',p[index]);
    }
    
    select = function(){
        var check = $(this).parent('ul').hasClass('on');

        var ul = $(this).parent('ul');
        var liLenght = $(this).parent('ul').children('li');
        var height = liLenght.length;
        var liIndex = liLenght.index(this);

        if(!check){
            ul.addClass('on');
            $(this).parents('.select').children('ul').css({height : 35})

            ul.css({height : 35 * height});
            $(this).css({top : 0 });
            $(this).parents('.select').addClass('active');
        }else{
            ul.removeClass('on');

            $(this).siblings('li').removeClass('active')
            $(this).addClass('active')
            ul.css({height : 35})
            $(this).css({top : (-35)* liIndex })
            $(this).parents('.select').removeClass('active');
        }
    }

    var gender = function(){
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
    }
    
    var inspect = function(e){
        
        //e.preventDefault(e);

        var id = $('#id').val();
        var pass = $('#pass').val();
        var pass_con = $('#pass_con').val();
        var email = $('#email').val();
        var phone = $('#phone').val()

        var err = $('<div>').attr('class','err');


        $('.err').remove();

        if(!id){
            $('#id').parent('.register-wrapper').append(err.append(' *아이디를 입력해주세요'));
            $('#id').focus();
            return;
        }
        
        if(!pass){
            $('#pass').parent('.register-wrapper').append(err.append(' *비밀번호를 입력해주세요'));
            $('#pass').focus();
            return;
        }
        
        if(!pass_con){
            $('#pass_con').parent('.register-wrapper').append(err.append(' *비밀번호 확인을 입력해주세요'));
            $('#pass_con').focus();
            return;
        }
        
        if(pass.length <= 7){
            $('#pass').parent('.register-wrapper').append(err.append(' *비밀번호는 8자리 이상입해력주세요. 숫자,문자,특수문자를 모두 사용하는것이 안전합니다'));
            $('#pass').focus();
            return;
        }

        if(pass != pass_con){
            $('#pass_con').parent('.register-wrapper').append(err.append(' *비밀번호가 일치하지 않습니다'));
            $('#pass_con').focus();
            return;
        }

        var regNumber = /^[0-9]*$/;
        if (!regNumber.test(phone)){
            $('#phone').parent('.register-wrapper').append(err.append(' *전화번호는 숫자만 가능합니다'));
            return;
        }
        
//        if(!$('#check').prop("checked")){
//            alert('체크박스에 체크 해주세요')
//            $('#check').focus();
//            return
//        }
        
        alert('추카추카');
        
    }
    
    var confirmation= function(){
        confirm('정말 가입을 멈추시겠습니까?');
    }
    
    return {
        focus : function(){
            $('input').on('focus',focusOn)
            $('input').on('focusout',focusOut)
        }, 
        click : function(){
            $('.select li').on('click', select);
            $('.gender div').on('click',gender);
        },
        join : function (){
            $('.join').on('click',inspect);
        },
        cancle : function (){
            $('.cancle').on('click',confirmation);
        }
    }  
 
}(jQuery));

var design= (function($){
    
    var up = function(){
        $(this).parent('.gender').removeClass('active');

    }
    
    var down = function(){
        $(this).parent('.gender').addClass('active');
    }
    
    return {
        click : function(){
            $('.gender div').on('mouseup',up);
            $('.gender div').on('mousedown',down);
        }
    }
    
}(jQuery));


$(document).ready(function(){
    func.focus();
    func.click();
    func.join();
    func.cancle();
    design.click();
});







    


