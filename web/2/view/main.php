<!-- section -->
<h2 class="blind">소개</h2>
<div class="section">
<div class="sec_wrap">
    <div class="con_wrap">
        <div class="inner">
<?php

                $dir = "../data/source/";//경로지정
                $handle  = opendir($dir);// 핸들 획득
                $files = array();// 디렉터리에 포함된 파일을 저장한다.
                while (false !== ($filename = readdir($handle))) {
                    if($filename == "." || $filename == ".." || $filename == "images"){
                        continue;
                    }

                    if(!is_file($dir . "/" . $filename)){// 파일인 경우만 목록에 추가한다.
                        $files[] = $filename;
                    }
                }
                closedir($handle);// 핸들 해제
                sort($files);// 정렬, 역순으로 정렬하려면 rsort 사용

                foreach ($files as $f) {// 파일명을 출력한다.
                $path =$dir.$f;
                $entrys = array();
                $dirs =dir($path);
                if(($entry != '.') && ($entry != '..') && empty($fileExt)){
                    $entrys['dir'][] = $entry;
                    $list = scandir($path);
                    $i = 0;
                        while($i < count($list)){
                            $title = htmlspecialchars($list[$i]);
                            $route = "../data/source/".$f."/".$title;
                                if(($list[$i] != '.') && ($list[$i] != '..')){
    ?>
            <div class="card coding">
                <div class="card_inner image">
                    <div class="front face" style="background-image:url('<?=$route?>/cover.jpg')">
                    </div>
                    <div class="face back">
                        <h2 class="title"><?=$title?></h2>
                        <div class="desc">
                            <h3>file down <a href="#">d</a></h3>
                            <p>comment</p>
                            <ul class="tag_list">
                                <li><?=$f?></li>
                            </ul>
                        </div>
                        <div class="link">
                            <a href="<?=$route?>" target="_blank"><span class="link_title">Web</span>link</a>
                        </div>
                    </div>
                </div>
            </div>

            <?php
                                }
                                $i = $i + 1;
                        }


                }
                }
?>

        </div>
    </div>
</div>
</div>

<div class="modal">
<div class="cover">
    <div class="cover_inner">
        <div class="m_wrap">
            <div class="m_head">head</div>
            <div class="m_body">
                <ul class="tag_list">
                </ul>
            </div>
            <div class="m_close">close</div>
        </div>
    </div>
</div>
</div>