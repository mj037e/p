<h2 class="blind">포트폴리오</h2>
<section class="section portfolio">
    <div class="sec_wrap">
        <h1>portfolio</h1>
        <p>스톤뮤직엔터테인먼트의 새로운 음악, 아티스트를 통해 <br>
        당신의 일상이 특별한 즐거움으로 채워집니다.</p>    
        <div class="photo">
            <ul>
<?php     

    $dis = array();
    $dis["title"] = array(
        '',
        '',
        '2017',        
        'copy.',
        '전진협',
        '학생관리 프로그램',
        'copy.mobile',
        'copy.mobile',

    );
    $dis["disc"] = array(
        '',
        '',
        '1page, scroll,resize',        
        '1page, slide',
        '2page, tab',
        '1page, 아코디언',
        '2page',
        '1page',

    );
          
    $entrys = array();
    if(($entry != '.') && ($entry != '..')){
		$entrys['dir'][] = $entry;
		//$sample = $_GET['mode'];
        $sample = "portfolio";
		$list = scandir($sample);
		$i = 0;
//		if(isset($_GET['mode'])){
			  while($i < count($list)){
				$title = htmlspecialchars($list[$i]);
				$route = $sample."/".$title;
                  
                
				if(($list[$i] != '.') && ($list[$i] != '..')){
?>                    
                <li style="background-image:url(<?=$route?>/cover.jpg)">
                    <a class="cover" href="<?=$route?>/<?=$f?>" alt="" target="_blank\">
                        <p class="cover_inner">
                            <span><?=$dis[title][$i]?></span>
                            <span><?=$dis[disc][$i]?></span>
                        </p>
                    </a>
                </li>
<?                    
				}
				$i = $i + 1;
                
                
			  }
//		 }   

    }
?>                
            </ul>
        </div>
        <div class="pf_list">
            <div class="list_title">TREND MAGAZINE</div>
            <div class="list_wrap">
              <div class="list_inner">
                  <div class="pf_nav cover">
                      <ul class="cover_inner">
                          <li class="active"></li>
                          <li></li>
                          <li></li>
                          <li></li>
                          <li></li>
                      </ul>
                  </div>
                  <ul class="pf_tex">
                      <li class="active">
                          <span class="sub">HOW TO STAY MINDFUL AF IN 2018</span>
                          <span class="tex">2018년, 마음을 잘 다스리는 방법1</span>
                      </li>
                      <li>
                          <span class="sub">HOW TO STAY MINDFUL AF IN 2018</span>
                          <span class="tex">2018년, 마음을 잘 다스리는 방법마음을 잘 다스리는 방법<span class="color">마음을 잘 다스리는 방법2</span></span>
                      </li>
                      <li>
                          <span class="sub">HOW TO STAY MINDFUL AF IN 2018</span>
                          <span class="tex">2018년, <span class="color">마음</span>을 잘 다스리는 방법3</span>
                      </li>
                      <li>
                          <span class="sub">HOW TO STAY MINDFUL AF IN 2018</span>
                          <span class="tex">2018년, 마음을 잘 다스리는 방법1마음을 잘 다스리는 방법4</span>
                      </li>
                      <li>
                          <span class="sub">HOW TO STAY MINDFUL AF IN 2018</span>
                          <span class="tex">2018년, 마음을 잘 다스리는 방법5</span>
                      </li>
                  </ul>
              </div>
            </div>
            <div class="btn">
                <button id="play">시작</button>
                <button id="pause">정지</button>
            </div>
        </div>
            
    </div>

</section>
