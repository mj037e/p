</div>
<div class="footer">
    <div class="footer_inner">
        <div class="top_f">
            <div class="logo left">eommi</div>
            <div class="right">
                <ul>
                    <li>
                        <object type="image/svg+xml" data="template/images/html.svg">
                            <p>사용하시는 Browser 는 이 그림을 표시하는 기능이 없습니다.
                                <a href="http://www.adobe.com/svg/viewer/install/main.html">SVG 플러그인</a>을 설치해보셔요.</P>
                        </object>
                    </li>
                    <li>
                        <object type="image/svg+xml" data="template/images/css.svg">
                            <p>사용하시는 Browser 는 이 그림을 표시하는 기능이 없습니다.
                                <a href="http://www.adobe.com/svg/viewer/install/main.html">SVG 플러그인</a>을 설치해보셔요.</P>
                        </object>
                    </li>
                    <li>
                        <object type="image/svg+xml" data="template/images/node.svg">
                            <p>사용하시는 Browser 는 이 그림을 표시하는 기능이 없습니다.
                                <a href="http://www.adobe.com/svg/viewer/install/main.html">SVG 플러그인</a>을 설치해보셔요.</P>
                        </object>
                    </li>
                    <li>
                        <object type="image/svg+xml" data="template/images/git.svg">
                            <p>사용하시는 Browser 는 이 그림을 표시하는 기능이 없습니다.
                                <a href="http://www.adobe.com/svg/viewer/install/main.html">SVG 플러그인</a>을 설치해보셔요.</P>
                        </object>
                    </li>
                </ul>
            </div>
        </div>
        <div class="bottom_f">
            <p><span>Copyright(C) 2018 eommi., All rights reserved.</span> <span>Contact Us</span> <span>Privacy</span></p>
        </div>
    </div>
    </div>
<div class="page_transition_effect"></div>
</body>
</html>
