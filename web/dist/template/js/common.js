var responsive;

function setResponsive() {
    if ($('div#media-479').css('display') == 'block') responsive = 1;
    else if ($('div#media-768').css('display') == 'block') responsive = 2;
    else if ($('div#media-1024').css('display') == 'block') responsive = 3;
    else if ($('div#media-1366').css('display') == 'block') responsive = 4;
    else responsive = 4;
}

$(window).on('resize', debounce(function(e) {
    setResponsive();
},100));

$(window).on('load', function () {
    $('.loader_moving').removeClass('on');
    setResponsive();

    var list = {
        coding : [
            {
                'url' : 'url(../../../portfolio/video/cover.jpg)',
                'link': '../portfolio/video',
                'type' :'coding portfolio',
                'title' :'basic',
                'by' : 'html · css',
                'txt': '웹의 기본인 HTML과 CSS를 작성/수정할 수 있습니다. 웹표준을 준수하여 작성합니다.'
            },
            {
                'url' : 'url(../../../portfolio/rotation_laout/cover.jpg)',
                'link': '../portfolio/rotation_laout',
                'type' :'coding portfolio',
                'title' :'Image Gallery',
                'by' : 'html · css · jquery',
                'txt': ' jQuery를 사용하여 웹에 생동감을 추가했습니다. 메뉴버튼을 누르면 액티브한 화면전환이 일어납니다.'
            },
            {
                'url' : 'url(../../../portfolio/scroll/cover.jpg)',
                'link': '../portfolio/scroll',
                'type' :'coding portfolio',
                'title' :'scroll animation',
                'by' : 'html · css · javascript',
                'txt': '스크롤 이벤트를 이용하여 애니메이션처럼 화면을 움직이는것을 보실 수 있습니다..'
            },
            {
                'url' : 'url(../../../portfolio/html/cover.jpg)',
                'link': '../portfolio/html',
                'type' :'coding portfolio',
                'title' :'website copy',
                'by' : 'html · css · javascript · plugin(swipe)',
                'txt': 'PHP MVC 기반 웹 개발 경험을 바탕으로 작업했습니다. Javascript로 메뉴, select_box, tab기능 등을 이용해 header에 있는 모든페이지를 구성했습니다.'
            }
            ],
        images : [
            {
                'image_link' : '../portfolio/design/product/ade_base/lemon_holic.jpg',
                'title' :'ade cover',
                'by' : 'photoshop',
            },
            {
                'image_link' : '../portfolio/design/product/gift_set/box2.jpg',
                'title' :'gift box',
                'by' : 'illustrator',
            },
            {
                'image_link' : '../portfolio/design/product/gift_set/teabag1-6.jpg',
                'title' :'teabag cover',
                'by' : 'illustrator',
            },
            {
                'image_link' : '../portfolio/design/product/event_sticker2.jpg',
                'title' :'sample sticker',
                'by' : 'photoshop',
            },
            {
                'image_link' : '../portfolio/design/product/leafTea_sticker1.jpg',
                'title' :'sample sticker',
                'by' : 'illustrator',
            },
            {
                'image_link' : '../portfolio/design/product/powder_sticker1.jpg',
                'title' :'powder sticker1',
                'by' : 'photoshop & illustrator',
            },
        ]
    };

    //contents 뿌리기
    (function(){

        var coding, images, contents_left, contents_right;
        coding = '\
         <div class="card">\
            <div class="card_image"><div class="card_image_inner"></div></div>\
            <div class="card_content">\
                <div class="card_inner">\
                    <div class="title">\
                        <p></p>\
                        <a href="#" target="_blank"></a>\
                    </div>\
                <div class="by"></div>\
                <div class="desc"></div>\
                </div>\
            </div>\
        </div>';

        images = '\
        <div class="card">\
            <div class="card_image">\
                <img />\
            </div>\
            <div class="card_content">\
                <div class="card_inner">\
                    <div class="title">\
                        <a href="#"></a>\
                    </div>\
                    <div class="by"></div>\
                </div>\
            </div>\
        </div>';

        contents_left = $('.contents.left');
        contents_right = $('.contents.right .card_wrap');
        for(var i = 0; i < list.coding.length;i++){
            contents_left.append(coding);
            $('.card',contents_left).eq(i).find('.card_image_inner').css('background-image',list.coding[i].url);
            $('.title p',contents_left).eq(i).text(list.coding[i].type);
            $('.title a',contents_left).eq(i).text(list.coding[i].title).attr('href',list.coding[i].link);
            $('.by',contents_left).eq(i).text(list.coding[i].by);
            $('.desc',contents_left).eq(i).text(list.coding[i].txt);
        }
        for(var i = 0; i < list.images.length;i++){
            contents_right.append(images);
            $('.card',contents_right).eq(i).find('img').attr('src',list.images[i].image_link);
            $('.title a',contents_right).eq(i).text(list.images[i].title);
            $('.by',contents_right).eq(i).text(list.images[i].by);
        }

    })();

    //image modal event
    (function(){

        var num = 0;

        $('.right').on('click','.card',function(e){
            var image , modal;
            modal = '\
                <div class="modal">\
                <div class= "cover">\
                    <div class= "cover_inner">\
                        <img class="image_view" />\
                        <p><span class="now"></span> / <span class="all"></span></p>\
                        <div class="prev">이전</div>\
                        <div class="next">다음</div>\
                    </div>\
                </div>\
                </div>\
            ';
            num = $(this).index();
            image = $('img',this).attr('src');
            $('.page_transition_effect').addClass('on').one('transitionend',function(){
                $('body').append(modal);
                $('.modal .image_view').attr('src',image);
                page();
            });

        });

        $('body').on('click','.modal',function(){
            $('.page_transition_effect').removeClass('on');
            $(this).remove();
        });
        $('body').on('click','.modal img',function(e){
            e.stopPropagation();
        });

        $('body').on('click','.modal .cover_inner div',function(e){
            e.stopPropagation();
            var class_name = $(this).attr('class');
            if(class_name === 'prev'){
                num = num-1;
                if(num <0){
                    num = 0;
                    alert('첫번째 이미지 입니다');
                }
            }else{
                num = num+1;
                console.log(num);
                console.log(list.images.length);
                if(num >= list.images.length){
                    num = list.images.length-1;
                    alert('마지막 이지미 입니다');
                }
            }
            $(this).siblings('img').attr('src',list.images[num].image_link);
            page();
        });
        function page(){
            $('.modal .now').text(num+1);
            $('.modal .all').text(list.images.length);
        }

    })();

    //hover event
    (function(){
        $('.contents').on('mouseenter','.card .title a',function(){
            // console.log($(this).parents('.card_content'));
            $(this).parent('.title').parent('.card_inner').parent('.card_content').siblings('.card_image').addClass('on');
        });
        $('.contents').on('mouseleave','.card .title a',function(){
            // console.log($(this).parents('.card_content'));
            $(this).parent('.title').parent('.card_inner').parent('.card_content').siblings('.card_image').removeClass('on');
        });
    })();

    (function(){
        $('.contents.left').on('click','.card_image',function() {
            var href = $(this).siblings('.card_content').find('a').attr('href');
            window.open(href, '_blank');
        });
    })();


});



function debounce(fn, delay) {
    var timer = null;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    };

};
