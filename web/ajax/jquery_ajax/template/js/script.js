$(document).ready(function() {

    var list = {
        route : 'view/',
        menu : [
            {
                'page_name' : 'page1',
                'title' : '제목1',
                'image_number' : 20,
                'file_name': '.png',
            },
            {
                'page_name' : 'page2',
                'title' : '제목2',
                'image_number' : 0,
                'file_name': '.png',
            },
        ],
    };


    for(var i = 0; i < list.menu.length; i++) {
        j = i + 1;
        if (j < 10) {
            j = '0' + j
        }
        $('.menu').append('<li>' + list.menu[i].title + '</li>');
    }

    //1.html 등 의 text 문서 읽기
    $('.menu li').each(function(i){

        $(this).on('click',function () {
            $("#content").hide().load(list.route+list.menu[i].page_name+'.html', function () {
                $(this).fadeIn(); // fade effect

                //이미지 넣기
                if(!(list.menu[i].image_number === 0)){
                    $(this).append('<div class="images_list"></div>');
                    for(var k = 0; k < list.menu[i].image_number ; k++){
                        j = k+1;
                        if(j < 10){
                            j = '0'+j
                        }
                        $('.images_list')
                            .append(
                                $('<div></div>'
                                ).css({
                                    'width':'100px',
                                    'height':'100px',
                                    'background-image':'url(template/data/page1/'+j+'.png)',
                                    'background-size':'cover'
                                }));
                    }
                }
            });
        });
    })

//        if(list.menu[i].image_number){
//            for(var i = 0; i < list.menu.image_number; i++){
//                j = i+1;
//                if(j < 10){
//                    j = '0'+j
//                }
//                $('.images_list')
//                    .append(
//                        $('<div>'+j+'</div>'
//                        ).css({
//                            'width':'100px',
//                            'height':'100px',
//                            'background-image':'url(template/data/page1/'+j+'.png)',
//                            'background-size':'cover'
//                        }));
//            }
//        }



});
