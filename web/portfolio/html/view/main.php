<!-- section -->
<h2 class="blind">abou me</h2>
<section class="section">
<div class="sec_wrap">
    <h3 class="title">Business Area</h3>
    <p class="subtitle">대림코퍼레이션은 지속적인 성장과 차별화된 고객가치를 실현하고 있습니다</p>
    <div class="visual_mask">

        <div class="swiper-container">

            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src="template/images/main/business_daelim2.png" alt=""/></div>
                <div class="swiper-slide"><img src="template/images/main/business_daelim2.png" alt=""/></div>
                <div class="swiper-slide"><img src="template/images/main/business_daelim2.png" alt=""/></div>
                <div class="swiper-slide"><img src="template/images/main/business_daelim2.png" alt=""/></div>
                <div class="swiper-slide"><img src="template/images/main/business_daelim2.png" alt=""/></div>
            </div>
            <!--<div class="mask_image">-->
                <!--<img src="template/images/main/mask_img01.png" alt=""/>-->
                <!--<img src="template/images/main/mask_img02.png" alt=""/>-->
                <!--<img src="template/images/main/mask_img03.png" alt=""/>-->
                <!--<img src="template/images/main/mask_img04.png" alt=""/>-->
                <!--<img src="template/images/main/mask_img05.png" alt=""/>-->
            <!--</div>-->
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
    </div>
</div>
</section>

    <!-- section -->
<h2 class="blind">About Company</h2>
<section class="section">
<div class="sec_wrap">
    <h3 class="title">About Company</h3>
    <p class="subtitle">대림코퍼레이션은 글로벌 디벨로퍼로의 미래 모습을 구현하기 위해 노력하고 있습니다.</p>
    <div class="aboutCompany">
        <div class="left">
            <ul>
                <li>
                    <a href="#">
                        <span class="years">1994~</span>
                        <p class="m_title">회사소개</p>
                        <p class="txt">대림코퍼레이션은 글로벌 디벨로퍼로의<br>미래 모습을 구현하기 위해 노력하고 있습니다.</p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <p class="m_title">재무정보</p>
                        <p class="txt">재무정보를 정확하고<br>신속하게 제공해 드립니다.</p>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <p class="m_title">인재채용</p>
                        <p class="txt">전세계를 향해 도전할 수 있는<br>글로벌 인재를 모십니다.</p>
                    </a>
                </li>
            </ul>
        </div>
        <div class="right">
            <div class="cover">
                <div class="cover_inner">
                    <div class="news">
                        <p class="n_title1"><span>회사소식</span> 대림코퍼레이션의 다양한 회사 소식을 알려 드립니다.</p>
                        <ul>
                            <li>
                                <div>
                                    <a href="#">
                                        <p class="n_title2">기준일 공고 안내</p>
                                        <p>
                                            기준일 공고

                                            회사의 업무와 관련한 통지를 하기로 결의하였으므로 이를 공고합니다.
                                            본 회사는 2018년 2월 6일자 이사회에서 2018년 2월 22일 현재 본 회사의 주주명부에 등재되어 있는 주주에게
                                            회사의 업무와 관련한 통지를 하기로 결의하였으므로 이를 공고합니다.

                                            2018년 2월
                                        </p>
                                    </a>
                                    <a href="#"><span class="date">2018-02-06</span></a>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <a href="#">
                                        <p class="n_title2">
                                            폴리이소부텐(PIB) 국내 대리점 모집 안내
                                        </p>
                                        <p>
                                            폴리이소부텐(PIB) 국내 대리점 모집

                                            폴리이소부텐(PIB) 국내 사업을 함께할 대리점을 아래와 같이 모집합니다.
                                            관심있는 분들의 많은 참여 바랍니다.
                                        </p>
                                    </a>
                                    <a href="#"><span class="date">2018-02-06</span> </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="icon_menu">
                        <ul>
                            <li>
                                <a href="#">
                                    <em>해외지사·국내지사</em>
                                    <p>넓은 세계를 향한 24시간 <br> 깨어 있는 대림코퍼레이션</p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <em>고객문의</em>
                                    <p>빠르고 신속하게 <br>답변해 드립니다.</p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <em>찾아오시는 길</em>
                                    <p>본사 위치 및 연락처를 <br> 안내해 드립니다.</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</section>

    <!-- section -->
<h2 class="blind">portfolio</h2>
<section class="section portfolio_wrap" style="display:none;">
<div class="sec_wrap">
    <ul class="content">
        <li>
            <a href="#">
                <div class="content_video"></div>
                <div class="content_text">
                    <h4>석유화학</h4>
                    <p></p>
                </div>
            </a>
        </li>
        <li>
            <a href="#">
                <div class="content_video"></div>
                <div class="content_text">
                    <h4>물류·해운</h4>
                    <p></p>
                </div>
            </a>
        </li>
        <li>
            <a href="#">
                <div class="content_video"></div>
                <div class="content_text">
                    <h4>건설정보화</h4>
                    <p></p>
                </div>
            </a>
        </li>
    </ul>
    <ul class="content">
        <li>
            <a href="#">
                <div class="content_video"></div>
                <div class="content_text">
                    <h4>IT 서비</h4>
                    <p></p>
                </div>
            </a>
        </li>
        <li>
            <a href="#">
                <div class="content_video"></div>
                <div class="content_text">
                    <h4>개발사업</h4>
                    <p></p>
                </div>
            </a>
        </li>
        <li>
            <a href="#">
                <div class="content_video"></div>
                <div class="content_text">
                    <h4>개발사업</h4>
                    <p></p>
                </div>
            </a>
        </li>
    </ul>

</div>

<div class="sec_wrap">
    <div class="cont_nav" id="footerMain" style="">
        <ul class="c_nav1">
            <li><a href="#">사업분야</a><span></span>
                <ul class="c_nav2">
                    <li><a href="#">석유화학</a></li>
                    <li><a href="#">물류·해운</a></li>
                    <li><a href="#">건설정보화</a></li>
                    <li><a href="#">IT서비스</a></li>
                    <li><a href="#">개발사업</a></li>
                </ul>
            </li>
            <li><a href="#">재무정보</a><span></span>
                <ul class="c_nav2">
                    <li><a href="#">재무제표</a></li>
                    <li><a href="#">공시정보</a></li>
                    <li><a href="#">공정거래자율준수</a></li>
                </ul>
            </li>
            <li><a href="#">자료실</a><span></span>
                <ul class="c_nav2">
                    <li><a href="#">자료실</a></li>
                    <li><a href="#">공시정보</a></li>
                    <li><a href="#">공정거래자율준수</a></li>
                </ul>
            </li>
            <li><a href="#">고객센터</a><span></span>
                <ul class="c_nav2">
                    <li><a href="#">일반문</a></li>
                    <li><a href="#">전자시스템</a></li>
                </ul>
            </li>

        </ul>
    </div>
</div>
</section>



