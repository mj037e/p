<div class="list_page">
    <div class="visual">

        <div class="cover">
            <div class="cover_inner">
                <h2>자료실</h2>
                <p>대림코퍼레이션 구성원들은 목표 의식을 가지고 끈기 있는 도전과 상호 간의 협력을 실천하여 <br/>  개인과 조직의 공동 성장을 추구합니다.</p>
            </div>
        </div>
    </div>
    <div class="content_wrap">
        <div class="bar">
            <div class="left">
                <select class="select" title="갯수">
                    <option value="15">15개씩 보기</option>
                    <option value="30">30개씩 보기</option>
                    <option value="60">60개씩 보기</option>
                </select>
            </div>
            <div class="right">
                <div class="search_box">
                    <select class="select" title="검색조건">
                        <option value="0">제목</option>
                        <option value="1">연도</option>
                    </select>
                    <input type="text" class="search_text" title="검색어"/>
                    <button type="button" class="search_btn">검색</button>
                </div>
            </div>
        </div>
        <div class="board_table">
            <table class="type2">
                <caption>재정운영</caption>
                <colgroup>
                    <col style="width:15%">
                    <col style="width:65%">
                    <col style="width:10%">
                    <col style="width:20%">
                </colgroup>
                <tbody>
                <tr>
                    <td class="num" scope="row">21</td>
                    <td>
                        <a href="#" class="title">2018년 특별교부금 교부 운용 기준</a>
                        <span class="icon new">
                            N
                            <em class="blind">NEW</em>
                        </span>
                    </td>
                    <td class="down">
                        <span class="blind">파일 다운로드</span>
                        <a href="#">
                            <span>28</span>
                        </a>
                    </td>
                    <td class="date">
                        <div class="double">
                            <div class="double_row">
                                <div class="cell">
                                    <span>회계연도</span>
                                    <em>2018</em>
                                </div>
                            </div>
                            <div class="double_row">
                                <div class="cell">
                                    <span>등록일자</span>
                                    <em>2018-04-24</em>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="num" scope="row">20</td>
                    <td>
                        <a href="#" class="title">2018년 특별교부금 교부 운용 기준</a>
                    </td>
                    <td class="down">
                        <span class="blind">파일 다운로드</span>
                        <a href="#">
                            <span>28</span>
                        </a>
                    </td>
                    <td class="date">
                        <div class="double">
                            <div class="double_row">
                                <div class="cell">
                                    <span>회계연도</span>
                                    <em>2018</em>
                                </div>
                            </div>
                            <div class="double_row">
                                <div class="cell">
                                    <span>등록일자</span>
                                    <em>2018-04-24</em>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="num" scope="row">19</td>
                    <td>
                        <a href="#" class="title">2018년 특별교부금 교부 운용 기준</a>
                    </td>
                    <td class="down">
                        <span class="blind">파일 다운로드</span>
                        <a href="#">
                            <span>28</span>
                        </a>
                    </td>
                    <td class="date">
                        <div class="double">
                            <div class="double_row">
                                <div class="cell">
                                    <span>회계연도</span>
                                    <em>2018</em>
                                </div>
                            </div>
                            <div class="double_row">
                                <div class="cell">
                                    <span>등록일자</span>
                                    <em>2018-04-24</em>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="num" scope="row">18</td>
                    <td>
                        <a href="#" class="title">2018년 특별교부금 교부 운용 기준</a>
                    </td>
                    <td class="down">
                        <span class="blind">파일 다운로드</span>
                        <a href="#">
                            <span>28</span>
                        </a>
                    </td>
                    <td class="date">
                        <div class="double">
                            <div class="double_row">
                                <div class="cell">
                                    <span>회계연도</span>
                                    <em>2018</em>
                                </div>
                            </div>
                            <div class="double_row">
                                <div class="cell">
                                    <span>등록일자</span>
                                    <em>2018-04-24</em>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="num" scope="row">17</td>
                    <td>
                        <a href="#" class="title">2018년 특별교부금 교부 운용 기준</a>
                    </td>
                    <td class="down">
                        <span class="blind">파일 다운로드</span>
                        <a href="#">
                            <span>28</span>
                        </a>
                    </td>
                    <td class="date">
                        <div class="double">
                            <div class="double_row">
                                <div class="cell">
                                    <span>회계연도</span>
                                    <em>2018</em>
                                </div>
                            </div>
                            <div class="double_row">
                                <div class="cell">
                                    <span>등록일자</span>
                                    <em>2018-04-24</em>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="num" scope="row">16</td>
                    <td>
                        <a href="#" class="title">2018년 특별교부금 교부 운용 기준</a>
                    </td>
                    <td class="down">
                        <span class="blind">파일 다운로드</span>
                        <a href="#">
                            <span>28</span>
                        </a>
                    </td>
                    <td class="date">
                        <div class="double">
                            <div class="double_row">
                                <div class="cell">
                                    <span>회계연도</span>
                                    <em>2018</em>
                                </div>
                            </div>
                            <div class="double_row">
                                <div class="cell">
                                    <span>등록일자</span>
                                    <em>2018-04-24</em>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="num" scope="row">15</td>
                    <td>
                        <a href="#" class="title">2018년 특별교부금 교부 운용 기준</a>
                    </td>
                    <td class="down">
                        <span class="blind">파일 다운로드</span>
                        <a href="#">
                            <span>28</span>
                        </a>
                    </td>
                    <td class="date">
                        <div class="double">
                            <div class="double_row">
                                <div class="cell">
                                    <span>회계연도</span>
                                    <em>2018</em>
                                </div>
                            </div>
                            <div class="double_row">
                                <div class="cell">
                                    <span>등록일자</span>
                                    <em>2018-04-24</em>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="num" scope="row">14</td>
                    <td>
                        <a href="#" class="title">2018년 특별교부금 교부 운용 기준</a>
                    </td>
                    <td class="down">
                        <span class="blind">파일 다운로드</span>
                        <a href="#">
                            <span>28</span>
                        </a>
                    </td>
                    <td class="date">
                        <div class="double">
                            <div class="double_row">
                                <div class="cell">
                                    <span>회계연도</span>
                                    <em>2018</em>
                                </div>
                            </div>
                            <div class="double_row">
                                <div class="cell">
                                    <span>등록일자</span>
                                    <em>2018-04-24</em>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="num" scope="row">13</td>
                    <td>
                        <a href="#" class="title">2018년 특별교부금 교부 운용 기준</a>
                    </td>
                    <td class="down">
                        <span class="blind">파일 다운로드</span>
                        <a href="#">
                            <span>28</span>
                        </a>
                    </td>
                    <td class="date">
                        <div class="double">
                            <div class="double_row">
                                <div class="cell">
                                    <span>회계연도</span>
                                    <em>2018</em>
                                </div>
                            </div>
                            <div class="double_row">
                                <div class="cell">
                                    <span>등록일자</span>
                                    <em>2018-04-24</em>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="num" scope="row">12</td>
                    <td>
                        <a href="#" class="title">2018년 특별교부금 교부 운용 기준</a>
                    </td>
                    <td class="down">
                        <span class="blind">파일 다운로드</span>
                        <a href="#">
                            <span>28</span>
                        </a>
                    </td>
                    <td class="date">
                        <div class="double">
                            <div class="double_row">
                                <div class="cell">
                                    <span>회계연도</span>
                                    <em>2018</em>
                                </div>
                            </div>
                            <div class="double_row">
                                <div class="cell">
                                    <span>등록일자</span>
                                    <em>2018-04-24</em>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="num" scope="row">11</td>
                    <td>
                        <a href="#" class="title">2018년 특별교부금 교부 운용 기준</a>
                    </td>
                    <td class="down">
                        <span class="blind">파일 다운로드</span>
                        <a href="#">
                            <span>28</span>
                        </a>
                    </td>
                    <td class="date">
                        <div class="double">
                            <div class="double_row">
                                <div class="cell">
                                    <span>회계연도</span>
                                    <em>2018</em>
                                </div>
                            </div>
                            <div class="double_row">
                                <div class="cell">
                                    <span>등록일자</span>
                                    <em>2018-04-24</em>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="num" scope="row">10</td>
                    <td>
                        <a href="#" class="title">2018년 특별교부금 교부 운용 기준</a>
                    </td>
                    <td class="down">
                        <span class="blind">파일 다운로드</span>
                        <a href="#">
                            <span>28</span>
                        </a>
                    </td>
                    <td class="date">
                        <div class="double">
                            <div class="double_row">
                                <div class="cell">
                                    <span>회계연도</span>
                                    <em>2018</em>
                                </div>
                            </div>
                            <div class="double_row">
                                <div class="cell">
                                    <span>등록일자</span>
                                    <em>2018-04-24</em>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="num" scope="row">9</td>
                    <td>
                        <a href="#" class="title">2018년 특별교부금 교부 운용 기준</a>
                    </td>
                    <td class="down">
                        <span class="blind">파일 다운로드</span>
                        <a href="#">
                            <span>28</span>
                        </a>
                    </td>
                    <td class="date">
                        <div class="double">
                            <div class="double_row">
                                <div class="cell">
                                    <span>회계연도</span>
                                    <em>2018</em>
                                </div>
                            </div>
                            <div class="double_row">
                                <div class="cell">
                                    <span>등록일자</span>
                                    <em>2018-04-24</em>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="num" scope="row">8</td>
                    <td>
                        <a href="#" class="title">2018년 특별교부금 교부 운용 기준</a>
                    </td>
                    <td class="down">
                        <span class="blind">파일 다운로드</span>
                        <a href="#">
                            <span>28</span>
                        </a>
                    </td>
                    <td class="date">
                        <div class="double">
                            <div class="double_row">
                                <div class="cell">
                                    <span>회계연도</span>
                                    <em>2018</em>
                                </div>
                            </div>
                            <div class="double_row">
                                <div class="cell">
                                    <span>등록일자</span>
                                    <em>2018-04-24</em>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="num" scope="row">7</td>
                    <td>
                        <a href="#" class="title">2018년 특별교부금 교부 운용 기준</a>
                    </td>
                    <td class="down">
                        <span class="blind">파일 다운로드</span>
                        <a href="#">
                            <span>9999</span>
                        </a>
                    </td>
                    <td class="date">
                        <div class="double">
                            <div class="double_row">
                                <div class="cell">
                                    <span>회계연도</span>
                                    <em>2018</em>
                                </div>
                            </div>
                            <div class="double_row">
                                <div class="cell">
                                    <span>등록일자</span>
                                    <em>2018-04-24</em>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="paging">
            <a href="#" class="first">
                <span class="blind">첫 페이지</span>
            </a>
            <a href="#" class="prev">
                <span class="blind">이전</span>
            </a>
            <a href="#" class="on">1</a>
            <a href="#">2</a>
            <a href="#" class="next">
                <span class="blind">다음</span>
            </a>
            <a href="#" class="last">
                <span class="blind">마지막 페이지</span>
            </a>
        </div>


    </div>
</div>