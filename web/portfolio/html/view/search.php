<style>
    .search .content_wrap{margin-top:0;}

    .wrap_search {overflow:hidden;}
    .wrap_search .box_search {text-align:center;}
    .wrap_search .field_search {width:525px;}
    .wrap_search .field_search input[type="text"] {width:428px;font-size:14px;color:#666666;text-indent:10px;}

    .box_search {padding:20px;border:1px solid #eaeaea;background:#f6f6f6;}
    .box_search {/*overflow:hidden;*/height:80px;text-align:right;line-height:0;font-size:0;}
    .box_search > * {display:inline-block;margin:0 0 0 10px;}

    .box_search .txt_search {margin:12px 0 0;float:left;font-size:14px;color:#777;line-height:1.2;}
    .box_search .txt_search:before {content:'';display:inline-block;vertical-align:middle;width:2px;height:2px;margin:-2px 4px 0 0;background:#7f7f7f;}
    .box_search .txt_search span {color:#2951ae;font-size:14px;vertical-align:top;}
    .field_search {width:330px;font-size:0;line-height:0;}
    .field_search > * {display:inline-block;vertical-align:middle;}
    .field_search input[type=text] {width:235px;height:40px;border:2px solid #1b2e5a;font-size:14px;}
    .field_search button[type=button] {width:95px;padding:0;background:#1b2e5a;font-size:14px;line-height:40px;color:#fff;vertical-align: top;}


</style>
<div class="search">
    <div class="content_wrap">
        <div class="sub_title">
            <h3>통합검색</h3>
        </div>
        <div class="wrap_search">
            <div class="box_search">
                <div class="field_search">
                    <label for="txtSrch" class="blind">검색어 입력</label>
                    <input id="txtSrch" type="text" placeholder="검색어를 입력 하세요!" value="">
                    <button type="button">검색</button>
                </div>
            </div>
        </div>




    </div>
</div>