$(document).ready(function(){
    $(function(){
    //btn 클릭하면 실행
    $('.btn').on('click',function(){
        $(this).fadeOut();
        $('.section,.nav').addClass('on');
    });

    //nav 메뉴 클릭하면 실행
    $('#gnb>li').on('click',function(){
        $('.btn').fadeIn();
        $('.section,.nav').removeClass('on');
        var i = $(this).index();
        $('.section>div').removeClass('on');
        $('.section>div').eq(i).addClass('on');
        });
    });  
});
